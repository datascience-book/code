include("ml_lib.jl")
include("dict_lib.jl")

function remove_stop_words(words, stop_words)
    copied_words = copy(words)
    for stop_word in stop_words
        deleteat!(copied_words, findall(x -> x == stop_word, copied_words))
    end
    copied_words
end

function text_to_unit_vector(text, stop_words)
    text_without_punctuation = replace(text, r"\W" => " ")
    words = split(lowercase(text_without_punctuation))
    stop_words_removed = remove_stop_words(words, stop_words)
    unit(counter(stop_words_removed))
end
