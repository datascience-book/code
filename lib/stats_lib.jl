include("lib/ml_lib.jl")

function sum(v::Vector)
    total = 0
    
    for element in v
        total += element
    end
    
    total
end


function vect_min(v::Vector)
    min = v[1]
    
    for element in v
        if element < min
            min = element
        end
    end
    
    return min
end


function vect_max(v::Vector)
    max = v[1]
    
    for element in v
        if element > max
            max = element
        end
    end
    
    return max
end


function vect_range(v::Vector)
    vect_max(v) - vect_min(v)
end


function mean(v::Vector)
    sum(v) / length(v)
end


function median(v::Vector)
    v = sort(v) 
    n = length(v)
    middle = Int(ceil(n/2))
    
    if n % 2 == 0
        (v[middle] + v[middle + 1]) / 2
    else
        v[middle]
    end
end


function mode(v::Vector)
    counts = counter(v)
    
    counts_max_to_min = sort([(val, key) for (key, val) in counts], rev = true)
    counts_max_to_min[1][2]
end


function percentile(v::Vector, p)
    index = trunc(Int, p * length(v))
    sort(v)[index]
end


function iqr(v::Vector)
    percentile(v, 0.75) - percentile(v, 0.25)
end


function variance(v::Vector, kind = :population)
    if kind == :population
        population_variance(v)
    else
        sample_variance(v)
    end
end


function population_variance(v::Vector)
    N = length(v)
    x = v
    μ = mean(v)
    mean_difference = x .- μ
    sq_mean_diff = mean_difference .^ 2
    sum(sq_mean_diff) / N
end


function sample_variance(v::Vector)
    n = length(v)
    x = v
    average = mean(v)
    avg_difference = x .- average
    sq_avg_diff = avg_difference .^ 2
    sum(sq_avg_diff) / (n -1)
end


function standard_deviation(v::Vector, kind = :population)
    variance = if kind == :population
        population_variance(v)
    else
        sample_variance(v)
    end
    
    sqrt(variance)
end


function covariance(x::Vector, y::Vector)
    x_mean = mean(x)
    y_mean = mean(y)
    x_diff_mean = x .- x_mean
    y_diff_mean = y .- y_mean
    
    n = length(x)
    sum(x_diff_mean .* y_diff_mean)  / (n - 1) # I have no clue about this n - 1
end


function correlation(x::Vector, y::Vector)
    covariance(x, y) / (standard_deviation(x, :sample) * standard_deviation(y, :sample))
end
