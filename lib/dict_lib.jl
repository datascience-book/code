function add(first_dictionaty::Dict, second_dictionary::Dict)
    all_keys = union(keys(first_dictionaty), keys(second_dictionary))
    
    output = Dict()
    
    for key in all_keys
        output[key] = get(first_dictionaty, key, 0) + get(second_dictionary, key, 0)
    end
    
    output
end

function mul(first_dictionaty::Dict, second_dictionary::Dict)
    all_keys = union(keys(first_dictionaty), keys(second_dictionary))
    
    output = Dict()
    
    for key in all_keys
        output[key] = get(first_dictionaty, key, 0) * get(second_dictionary, key, 0)
    end
    
    output
end

function mul(dictionary::Dict, number)
    output = Dict()
    
    for key in keys(dictionary)
        output[key] = dictionary[key] * number
    end
    
    output
end

function inverse(dictionary::Dict)
    output = Dict()
    
    for key in keys(dictionary)
        output[key] = 1 / dictionary[key]
    end
    
    output
end

function divide(first_dictionaty::Dict, second_dictionary::Dict)
    all_keys = union(keys(first_dictionaty), keys(second_dictionary))
    
    output = Dict()
    
    for key in all_keys
        output[key] = get(first_dictionaty, key, 0) / get(second_dictionary, key, 0)
    end
    
    output
end

function magnitude(dictionary::Dict)
    sqrt(sum(values(dictionary) .^ 2))
end

function divide(dictionary::Dict, number)
    mul(dictionary, 1/number)
end

function unit(dictionary::Dict)
    divide(dictionary, magnitude(dictionary))
end

function dot(first_dictionary::Dict, second_dictionary::Dict)
    multiplied = mul(first_dictionary, second_dictionary)
    sum(values(multiplied))
end

function subtract(first_dictionary::Dict, second_dictionary::Dict)
    add(first_dictionary, mul(second_dictionary, -1))
end
