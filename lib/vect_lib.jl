function midpoint(array_of_vectors::Vector)
    reduce(+, array_of_vectors) / length(array_of_vectors)
end

function distance(first_vector::Vector, second_vector::Vector)
    Δ = first_vector - second_vector
    sum_of_squares = sum(Δ .^ 2)
    sqrt(sum_of_squares)
end

function magnitude(vector::Vector)
    sqrt(sum(vector .^ 2))
end

function unit(vector::Vector)
    vector / magnitude(vector)
end
